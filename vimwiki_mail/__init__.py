from contextlib import contextmanager
import logging
import poplib
import email
import quopri
from pathlib import Path


_log = logging.getLogger(__name__)


@contextmanager
def POPServer(hostname, username, password):
    """ Connection to POP3 server (for `with`).
    """
    _log.info("connecting to %s", hostname)
    pop3 = poplib.POP3_SSL(hostname)
    assert pop3.user(username).startswith(b"+OK")
    assert pop3.pass_(password).startswith(b"+OK")
    try:
        yield pop3
    finally:
        _log.info("disconnecting from %s", hostname)
        pop3.quit()


def get_mails(pop3):
    """ Get all mails from inbox of POP3 connection, (msg_id, message) pairs.
    """
    mails = []
    status, msg_ids, _size = pop3.list()
    assert status.startswith(b"+OK")
    for msg_id in msg_ids:
        msg_id, _, __ = msg_id.decode("utf-8").partition(" ")
        status, msg, _size = pop3.retr(msg_id)
        assert status.startswith(b"+OK")
        mails.append((msg_id, email.message_from_bytes(b"\n".join(msg))))
    return mails


def get_mail_pieces(mail):
    """ Split up mail message into constituent parts:
        From header, subject, message body, signature, and attachments.
    """
    subject = mail.get("Subject")
    from_ = mail.get("From")
    body, body_charset = "", "utf-8"
    attachments = []
    for part in mail.walk():
        content_type = part.get_content_type()
        if content_type == "text/plain":
            body = quopri.decodestring(part.get_payload(decode=True))
            body_charset = part.get_content_charset()
        elif content_type.startswith("multipart/") or content_type in (
            "text/html",
            "text/rtf",
        ):
            pass
        else:
            # _log.debug("content type: %s", part.get_content_type())
            attachments.append((part.get_filename(), part.get_payload(decode=True)))
    body, _, signature = body.decode(body_charset).partition("\n-- \n")
    return from_, subject, body or None, signature or None, attachments


class VimWikiPath:
    """ This class encapsulates everything path related to VimWiki pages
        and their contents and attachments. The bases are a) the GIT base
        directory of the VimWiki, and the mail's subject.

        Example:
        basedir = /home/schemitz/Organisation/vimwiki
        subject = VimWikiMail or VimWikiMail/Testdoc
        attachmant = some_screenshot.jpg

        -> path() = /home/schemitz/Organisation/vimwiki/VimWikiMail.md or /home/schemitz/Organisation/vimwiki/VimWikiMail/Testdoc.md
        -> attachment_path() = /home/schemitz/Organisation/vimwiki/VimWikiMail/some_screenshot.jpg
    """

    def __init__(self, basedir, subject):
        self.basedir = basedir
        self.parts = [s.strip() for s in subject.split("/")]

    def path(self):
        p = Path(self.basedir, *(self.parts[:-1]), self.parts[-1] + ".md")
        assert p.relative_to(self.basedir)
        return p

    def attachment_path(self, att_fname):
        p = Path(self.basedir, *self.parts, att_fname)
        assert p.relative_to(self.basedir)
        return p
