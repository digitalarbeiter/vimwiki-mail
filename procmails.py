#!/usr/bin/env python3

import os
import re
import email
import logging
import click
import configparser
from pathlib import Path
from contextlib import contextmanager

from vimwiki_mail import POPServer, get_mails, get_mail_pieces, VimWikiPath


_log = logging.getLogger(__name__)


@contextmanager
def git_commit(basedir, commit_msg, dryrun=False):
    """ Commit changes to `basedir` in the managed context, using the
        `commit_msg`.
    """
    commit_msg = re.sub(r"[^\w@.]+", " ", commit_msg).strip()
    _log.info(f"git transaction in {basedir} begins")
    if not dryrun:
        os.system(f"cd '{basedir}' && git pull")
    try:
        yield
        _log.info(f"git commit {commit_msg}")
        if not dryrun:
            os.system(
                f"cd '{basedir}' && git add . && git commit -m '{commit_msg}' && git push"
            )
    except:
        _log.warning("git rollback")
        if not dryrun:
            os.system(f"cd '{basedir}' && git reset --hard")
        raise


def switch(it, cond):
    """ Split the items in `it` into two lists: one with the elements
        fulfilling the `cond`, the other with the elements not fulfilling
        `cond`.
    """
    a, b = [], []
    for item in it:
        if cond(item):
            a.append(item)
        else:
            b.append(item)
    return a, b


def decoded_header(mail, field):
    """ Mail headers might contain really confusingly encoded text, like
        =?UTF-8?Q?todo_+haushalt_project=3afear_+brexit_Kl=c3=b6papier_e
        and shit like that, so we gotta decode that, but decode_header()
        doesn't really decode but only figure out the encoding:
        [('task +haushalt Einkaufen im dm', None)]
        [(b'todo +haushalt project:fear +brexit Kl\xc3\xb6papier eink\xc3\xa4ufen', 'utf-8')]
    """
    text, encoding = email.header.decode_header(mail.get(field))[0]
    return text.decode(encoding) if encoding else text


def md_header(txt, level=1):
    """ Format `txt` as a Markdown header.
    """
    return "#" * level + txt + "\n"


def md_resource(path):
    """ Format file `path` as a Markdown resource (e.g. an image).
    """
    dot = path.parts[-1].rfind(".")
    if dot == -1:
        alt_text = path.parts[-1]
    else:
        alt_text = path.parts[-1][:dot]
    alt_text = alt_text.replace("_", " ")
    rel_path = Path(*(path.parts[-2:]))
    return f"![{alt_text}]({rel_path})\n"


def mail_to_vimwiki(basedir, mail):
    """ Create (or update) `mail`s subject VimWiki page with `mail`s content.
        Attachments are added to the corresponding directory.
    """
    from_, subject, body, signature, attachments = get_mail_pieces(mail)
    mail_path = VimWikiPath(basedir, subject)
    body_path = mail_path.path()
    if body:
        _log.info(f"appending body to: {body_path}")
        body_path.parent.mkdir(parents=True, exist_ok=True)
        with open(body_path, "a") as doc:
            doc.write(f"\n{md_header(subject)}\n")
            doc.write(f"\n{body}\n")
    for filename, payload in attachments:
        path = mail_path.attachment_path(filename)
        _log.debug(f"storing attachment to: {path}")
        path.parent.mkdir(parents=True, exist_ok=True)
        with open(path, "wb") as att:
            att.write(payload)
        with open(body_path, "a") as doc:
            doc.write(md_resource(path) + "\n")


def mail_to_taskwarrior(subject):
    """ Create TaskWarrior task from mail's `subject` line. Allow for
        +tags and project: fields.
    """
    subject = subject[4:]  # strip leading "task"/"todo"
    subject = subject.strip(":").strip().strip(":").strip()
    subject = subject.replace("'", " ").replace('"', " ").replace("\\", " ")
    words = [w for w in subject.split() if w]
    tags, words = switch(words, lambda w: w.startswith("+"))
    projects, words = switch(words, lambda w: w.startswith("project:"))
    title = " ".join(words)
    tags = ["+" + re.sub(r"\W+", "", t) for t in tags]
    assert len(projects) <= 1
    if projects:
        _, __, project = projects[0].partition(":")
        project = re.sub(r"\W+", "", project)
        project = f"project:{project}"
    else:
        project = ""
    _log.info(f"new task: {title}")
    os.system(f"task add {project} {' '.join(tags)} '{title}'")


@click.command()
@click.option("--server", help="POP3 server from which to get mails")
@click.option("--username", help="POP3 username on --server, see --password")
@click.option("--password", help="POP3 password on --server, see --username")
@click.option("--basedir", help="directory of VimWiki GIT repository")
@click.option("--keep/--delete", help="--keep mails on server, or --delete them", is_flag=True, required=True, default=True)
@click.option("--verbose", help="be more verbose in output", is_flag=True)
@click.option("--debug", help="be really, really verbose in output", is_flag=True)
def cli(server, username, password, basedir, keep, verbose, debug):
    level = logging.DEBUG if debug else logging.INFO if verbose else logging.WARNING
    logging.basicConfig(level=level, format="%(asctime)s %(levelname)-7s %(message)s")
    config = configparser.ConfigParser()
    configs = config.read(os.environ["HOME"] + "/.mail2vimwikirc")
    assert len(configs) == 1  # mssing config file
    if not server:
        server = config["mail"]["server"]
    if not username:
        username = config["mail"]["username"]
    if not password:
        password = config["mail"]["password"]
    if not basedir:
        basedir = config["git"]["basedir"]
    with POPServer(server, username, password) as pop3:
        for msg_id, mail in get_mails(pop3):
            sender = decoded_header(mail, "From")
            sender = [w for w in sender.split() if "@" in w][-1]
            commit_msg = f"{sender} via mail"
            with git_commit(basedir, commit_msg, dryrun=False):
                subject = "<invalid subject>"
                try:
                    subject = decoded_header(mail, "Subject")
                    _log.info(f"subject: {subject}")
                    if subject.lower().startswith("todo") or subject.lower().startswith(
                        "task"
                    ):
                        mail_to_taskwarrior(subject)
                    else:
                        mail_to_vimwiki(basedir + "/vimwiki", mail)
                    if not keep:
                        pop3.dele(msg_id)
                except:
                    _log.error(f"failed to process mail {msg_id}: {subject}")
                    raise


if __name__ == "__main__":
    cli()
